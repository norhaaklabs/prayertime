#!/usr/bin/env python

from bs4 import BeautifulSoup
from cache import Cache
from datetime import datetime, timedelta
import requests


class PrayerTime:
    def __init__(self):
        pass

    def convert_time_to_utc(self, date_str, time_str):
        datetime_str = '{} {}'.format(date_str, time_str)
        datetime_ = datetime.strptime(datetime_str, '%Y-%m-%d %H:%M') - timedelta(hours=1)
        time_utc = datetime_.strftime('%H:%M')
        return time_utc


    def build_prayer_times(self, prayer_names, prayer_times):
        prayer_times_list = []
        for i in range(len(prayer_names)):
            prayer_time = {
                'name': prayer_names[i],
                'time': prayer_times[i]
            }
            prayer_times_list.append(prayer_time)
        return prayer_times_list

    def parse_parayer_times(self, city_name):
        cache_entries = []
        now = datetime.now()
        cache = Cache()
        url = 'https://lematin.ma/horaire-priere-{}.html'.format(city_name)
        page = requests.get(url, verify=False)
        if page.status_code == 200:
            soup = BeautifulSoup(page.content, 'html.parser')
            table = soup.find('table', {'class': 'table table-striped'})
            rows = table.findAll('tr')
            header = [h.text for h in rows[0].findAll('th')]
            prayer_names = header[1:]
            for row in rows[1:]:
                times = [d.text for d in row.findAll('td')]
                day_of_month = int(times[0])
                prayer_times = times[1:]
                date_str = '{}-{:02}-{:02}'.format(now.year, now.month, day_of_month)
                prayer_times_list = self.build_prayer_times(prayer_names, prayer_times)
                cache_entry = {
                    'date': date_str,
                    'prayer_times': prayer_times_list
                }
                cache_entries.append(cache_entry)
            for cache_entry in cache_entries:
                id = '{}{}'.format(city_name, cache_entry['date'])
                cache.put(id, cache_entry)
            cache.save()


    def get_prayer_times(self, city_name, date_=datetime.now()):
        prayer_times = None
        date_str = date_.strftime('%Y-%m-%d')
        id = '{}{}'.format(city_name, date_str)
        cache = Cache()
        if not cache.get(id):
            self.parse_parayer_times(city_name)
        cache_entry = cache.get(id)
        prayer_times = cache_entry['prayer_times']
        date_str = cache_entry['date']
        return (date_str, prayer_times)


    def timedelta2str(self, time_left):
        total_seconds = time_left.total_seconds()
        hours = int(total_seconds // 3600)
        minutes = int((total_seconds % 3600) // 60)
        if hours > 0:
            result = '{} H:{} Min'.format(hours, minutes)
        else:
            result = '{} Min'.format(minutes)
        return result


    def next_prayer(self, prayer_times):
        now = datetime.now()
        prayer_times = [datetime.strptime(pt, '%Y-%m-%d %H:%M') for pt in prayer_times]
        next_prayers = [pt for pt in prayer_times if pt >= now]
        if len(next_prayers) >= 1:
            next_prayer = next_prayers[0]
            time_left = next_prayer - now
            print('Time left for next prayer: {}'.format(self.timedelta2str(time_left)))
        else:
            print('No prayer left for today.')

if __name__ == '__main__':
    city_name = 'casablanca'
    pt = PrayerTime()
    date_str, prayer_times = pt.get_prayer_times(city_name)
    print('######### Prayer times for {} [{}] #########'.format(city_name.capitalize(), date_str))
    today_prayer_times = []
    for prayer_time in prayer_times:
        prayer_time_str = pt.convert_time_to_utc(date_str, prayer_time['time'])
        today_prayer_times.append('{} {}'.format(date_str, prayer_time_str))
        print("{:<15}: {}".format(prayer_time['name'], prayer_time_str))
    pt.next_prayer(today_prayer_times)