#!/usr/bin/env python

from hashlib import sha1
import json
import os


class Cache:
    def __init__(self):
        self.cache_db_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'cache.db')
        self.load()

    def load(self):
        self.cached_content = {}
        try:
            with open(self.cache_db_path, 'r') as f:
                content = f.read()
                self.cached_content = json.loads(content)
        except:
            pass


    def save(self):
        with open(self.cache_db_path, 'w') as f:
            f.write(json.dumps(self.cached_content, indent=2))

    def get(self, id):
        id_ = self.getCheksum(id)
        if (id_ in self.cached_content.keys()):
            return self.cached_content[id_]
        else:
            return None

    def put(self, id, prayer_time):
        id_ = self.getCheksum(id)
        self.cached_content[id_] = prayer_time

    def getCheksum(self, value):
        hash = sha1()
        hash.update(value.encode('UTF-8'))
        return hash.hexdigest()