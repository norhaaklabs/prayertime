#!/usr/bin/env python

from prayertime import PrayerTime
from datetime import datetime, timedelta

#TODO build user menu to diplay list of cities
#TODO download monthly prayer times

if __name__ == '__main__':
    city_name = 'casablanca'
    pt = PrayerTime()
    date_str, prayer_times = pt.get_prayer_times(city_name)
    print('######### Prayer times for {} [{}] #########'.format(city_name.capitalize(), date_str))
    today_prayer_times = []
    for prayer_time in prayer_times:
        prayer_time_str = pt.convert_time_to_utc(date_str, prayer_time['time'])
        today_prayer_times.append('{} {}'.format(date_str, prayer_time_str))
        print("{:<15}: {}".format(prayer_time['name'], prayer_time_str))
    pt.next_prayer(today_prayer_times)